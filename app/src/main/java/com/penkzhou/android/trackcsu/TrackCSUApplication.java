package com.penkzhou.android.trackcsu;

import android.app.Application;

import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.baidu.mapapi.SDKInitializer;
import com.penkzhou.android.trackcsu.model.CSULocation;

/**
 * Created by Administrator on 2014/11/8.
 */
public class TrackCSUApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        SDKInitializer.initialize(getApplicationContext());
        AVObject.registerSubclass(CSULocation.class);
        AVOSCloud.initialize(this, "0yr43l0x4x4nkxtcki2o530rw435b4tr8j786w1f0j8ev6in", "cui0trpe57l3lchldhsb7cwh9wqo9vx4m1pqhg7x6arxxjsj");
    }
}
