package com.penkzhou.android.trackcsu.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.SaveCallback;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.penkzhou.android.trackcsu.R;
import com.penkzhou.android.trackcsu.model.CSULocation;
import com.penkzhou.android.trackcsu.ui.TrackMapActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TrackCSUService extends Service implements BDLocationListener {
    private final static String TAG = TrackCSUService.class.getSimpleName();
    private final static int INTERNAL_TIME = 40;
    private static Context context;
    public LocationClient mLocationClient;
    private boolean currentlyProcessingLocation = false;
    private int notificationID = 1;
    private Notification foregroundNotification;
    private Intent notificationIntent;
    private String lastSaveTimeStr;
    private PendingIntent pendingIntent;

    private static void sendMessageToFragment(BDLocation l, String msg) {
        Intent intent = new Intent("GPSLocationUpdates");
        // You can also include some extra data.
        intent.putExtra("Status", msg);
        Bundle bundle = new Bundle();
        bundle.putParcelable("Location", l);
        intent.putExtra("Location", bundle);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate FUCK XXXXXXXXXXXXXXXXX");
        super.onCreate();
        this.context = getApplicationContext();
        prepareNotification();
    }

    private void prepareNotification() {
        foregroundNotification = new Notification(R.drawable.ic_launcher, getText(R.string.track_csu_notification),
                System.currentTimeMillis());
        notificationIntent = new Intent(this, TrackMapActivity.class);
        pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand FUCK XXXXXXXXXXXXXXXXX");
        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTrackCSU();
        }
        return START_NOT_STICKY;
    }

    private void startTrackCSU() {
        Log.d(TAG, "startTrackCSU");
        mLocationClient = new LocationClient(getApplicationContext());
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationClientOption.LocationMode.Battery_Saving);//设置定位模式
        //option.setCoorType("bd09ll");//返回的定位结果是百度经纬度,默认值gcj02
        option.setScanSpan(5000);//设置发起定位请求的间隔时间为5000ms
        //option.setIsNeedAddress(true);//返回的定位结果包含地址信息
        mLocationClient.setLocOption(option);
        mLocationClient.registerLocationListener(this);    //注册监听函数
        mLocationClient.start();

        foregroundNotification.setLatestEventInfo(this, getText(R.string.notification_title),
                getText(R.string.notification_message), pendingIntent);
        startForeground(notificationID, foregroundNotification);
        if (mLocationClient != null && mLocationClient.isStarted()) {
            mLocationClient.requestLocation();
        } else
            Log.d("LocSDK4", "locClient is null or not started");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy FUCK XXXXXXXXXXXXXXXXX");
        super.onDestroy();
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onReceiveLocation(final BDLocation bdLocation) {
        saveLocation(bdLocation);
        sendMessageToFragment(bdLocation, "定位成功");
        foregroundNotification.setLatestEventInfo(this, getText(R.string.notification_title),
                getString(R.string.notification_message, bdLocation.getLongitude(), bdLocation.getLatitude()), pendingIntent);
        startForeground(notificationID, foregroundNotification);
    }

    public void saveLocation(final BDLocation bdLocation) {
        CSULocation csuLocation = new CSULocation();
        csuLocation.setLatitude(bdLocation.getLatitude());
        csuLocation.setLongitude(bdLocation.getLongitude());
        csuLocation.setTimeout("false");
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date locTime = null;
        try {
            locTime = sdfDate.parse(bdLocation.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        csuLocation.setLocTime(locTime);
        final SharedPreferences sharedPreferences = this.getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        csuLocation.setUsername(sharedPreferences.getString("userName", ""));
        isIntheThreeMinutes();
        if (compareLocation(bdLocation) || isIntheThreeMinutes()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            putDouble(editor, "lastLatitude", bdLocation.getLatitude());
            putDouble(editor, "lastLongitude", bdLocation.getLongitude());
            editor.apply();
            csuLocation.saveInBackground(new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (e == null) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("lastSaveTimeStr", getCurrentTimeString());
                        editor.apply();
                        Log.d(TAG, "logdate_save : " + bdLocation.getTime() + "lastLatitude:" + bdLocation.getLatitude() + "lastLongitude:" + bdLocation.getLongitude());
                        Log.d(TAG, "Locate Success : Longitude : " + bdLocation.getLongitude() + " ; Latitude : " + bdLocation.getLatitude() + "; Locate time: " + bdLocation.getTime());
                    } else {
                        Log.d(TAG, e.getMessage() + e.toString());
                        Log.d(TAG, "Locate Error : Longitude : " + bdLocation.getLongitude() + " ; Latitude : " + bdLocation.getLatitude() + "; Locate time: " + bdLocation.getTime());
                    }
                }
            });
        }
    }

    public String getCurrentTimeString() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        return sdfDate.format(now);
    }

    public boolean isIntheThreeMinutes() {
        final SharedPreferences sharedPreferences = this.getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        lastSaveTimeStr = sharedPreferences.getString("lastSaveTimeStr", getCurrentTimeString());
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date lastSaveDate = null;
        try {
            lastSaveDate = sdfDate.parse(lastSaveTimeStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date now = new Date();
        long seconds = (now.getTime() - lastSaveDate.getTime()) / 1000;
        Log.d(TAG, "logdate_second : " + seconds);
        if (seconds > INTERNAL_TIME) {
            return true;
        } else {
            return false;
        }
    }

    public boolean compareLocation(BDLocation bdLocation) {
        final SharedPreferences sharedPreferences = this.getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        Log.d(TAG, "logdate_save : " + bdLocation.getTime() + "prefLatitude:" + getDouble(sharedPreferences, "lastLatitude", 0) + "prefLongitude:" + getDouble(sharedPreferences, "lastLongitude", 0));
        return getDouble(sharedPreferences, "lastLatitude", 0) != bdLocation.getLatitude() || getDouble(sharedPreferences, "lastLongitude", 0) != bdLocation.getLongitude();
    }

    SharedPreferences.Editor putDouble(final SharedPreferences.Editor edit, final String key, final double value) {
        return edit.putLong(key, Double.doubleToRawLongBits(value));
    }

    double getDouble(final SharedPreferences prefs, final String key, final double defaultValue) {
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(defaultValue)));
    }
}
