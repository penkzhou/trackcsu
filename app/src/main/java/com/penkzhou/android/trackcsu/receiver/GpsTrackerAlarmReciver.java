package com.penkzhou.android.trackcsu.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.penkzhou.android.trackcsu.service.TrackCSUService;

/**
 * Created by Administrator on 2014/11/8.
 */
public class GpsTrackerAlarmReciver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, TrackCSUService.class));
    }
}
