package com.penkzhou.android.trackcsu.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.penkzhou.android.trackcsu.R;
import com.penkzhou.android.trackcsu.receiver.GpsTrackerAlarmReciver;

/**
 * Created by Administrator on 2014/11/8.
 * reference from https://github.com/nickfox/GpsTracker/.
 */
public class TrackMapFragment extends Fragment {
    private static final String TAG = TrackMapFragment.class.getSimpleName();

    private Button startButton;
    private EditText txtUserName;
    private boolean currentlyTracking;
    private AlarmManager alarmManager;
    private Intent gpsTrackerIntent;
    private TextView stateTextView;
    private TextView longtitudeTextView;
    private TextView latitudeTextView;
    private TextView locateTimeTextView;
    private TextView radiusTextView;
    private TextView addressTextView;
    private PendingIntent pendingIntent;
    private BDLocation lastKnownLoc;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("Status");
            Bundle b = intent.getBundleExtra("Location");
            lastKnownLoc = (BDLocation) b.getParcelable("Location");
            if (lastKnownLoc != null && isAdded()) {
                stateTextView.setText(getString(R.string.locate_state_desc, message));
                locateTimeTextView.setText(getString(R.string.locate_time_desc, lastKnownLoc.getTime()));
                longtitudeTextView.setText(getString(R.string.longtitude_desc, lastKnownLoc.getLongitude()));
                latitudeTextView.setText(getString(R.string.latitude_desc, lastKnownLoc.getLatitude()));
                //addressTextView.setText(getString(R.string.location_desc, lastKnownLoc.getAddrStr()));
                radiusTextView.setText(getString(R.string.radius_desc, lastKnownLoc.getRadius()));
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mMessageReceiver, new IntentFilter("GPSLocationUpdates"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_trackmap, container, false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        currentlyTracking = sharedPreferences.getBoolean("currentlyTracking", false);
        currentlyTracking = sharedPreferences.getBoolean("currentlyTracking", false);
        stateTextView = (TextView) rootView.findViewById(R.id.locate_state_desc);
        locateTimeTextView = (TextView) rootView.findViewById(R.id.locate_time_desc);
        longtitudeTextView = (TextView) rootView.findViewById(R.id.longtitude_desc);
        latitudeTextView = (TextView) rootView.findViewById(R.id.latitude_desc);
        radiusTextView = (TextView) rootView.findViewById(R.id.radius_desc);
        //addressTextView = (TextView) rootView.findViewById(R.id.address_desc);

        stateTextView.setText(getString(R.string.locate_state_desc, getString(R.string.locate_default)));
        locateTimeTextView.setText(getString(R.string.locate_time_desc, getString(R.string.locate_default)));
        longtitudeTextView.setText(getString(R.string.longtitude_desc, getString(R.string.locate_default)));
        latitudeTextView.setText(getString(R.string.latitude_desc, getString(R.string.locate_default)));
        //addressTextView.setText(getString(R.string.location_desc, getString(R.string.locate_default)));
        radiusTextView.setText(getString(R.string.radius_desc, getString(R.string.locate_default)));
        startButton = (Button) rootView.findViewById(R.id.btnStart);
        txtUserName = (EditText) rootView.findViewById(R.id.userName);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackLocation(v);
            }
        });

        return rootView;
    }

    private void startAlarmManager() {
        Log.d(TAG, "startAlarmManager");

        Context context = getActivity();
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReciver.class);
        pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                60000, // 60000 = 1 minute
                pendingIntent);
    }

    private void cancelAlarmManager() {
        Log.d(TAG, "cancelAlarmManager");

        Context context = getActivity();
        Intent gpsTrackerIntent = new Intent(context, GpsTrackerAlarmReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, gpsTrackerIntent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    protected void trackLocation(View v) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (!saveUserSettings()) {
            return;
        }
        if (currentlyTracking) {
            Toast.makeText(getActivity(), "已经停止定位！", Toast.LENGTH_LONG).show();
            cancelAlarmManager();
            currentlyTracking = false;
            editor.putBoolean("currentlyTracking", false);
        } else {
            Toast.makeText(getActivity(), "开始定位……", Toast.LENGTH_LONG).show();
            startAlarmManager();
            currentlyTracking = true;
            editor.putBoolean("currentlyTracking", true);
        }
        editor.apply();
        setTrackingButtonState();
    }

    private boolean saveUserSettings() {
        if (textFieldsAreEmptyOrHaveSpaces()) {
            return false;
        }
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userName", txtUserName.getText().toString().trim());

        editor.apply();

        return true;
    }

    private boolean textFieldsAreEmptyOrHaveSpaces() {
        String tempUserName = txtUserName.getText().toString().trim();

        if (tempUserName.length() == 0 || hasSpaces(tempUserName)) {
            Toast.makeText(getActivity(), R.string.textfields_empty_or_spaces, Toast.LENGTH_LONG).show();
            return true;
        }

        return false;
    }

    private boolean hasSpaces(String str) {
        return ((str.split(" ").length > 1) ? true : false);
    }


    private void setTrackingButtonState() {
        if (currentlyTracking) {
            startButton.setText("定位中");
            startButton.setBackgroundColor(Color.GREEN);
        } else {
            startButton.setText("定位停止.");
            startButton.setBackgroundColor(Color.RED);
        }
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        displayUserSettings();
        setTrackingButtonState();
    }

    private void displayUserSettings() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("com.penkzhou.android.trackcsu.prefs", Context.MODE_PRIVATE);
        txtUserName.setText(sharedPreferences.getString("userName", ""));
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

}
