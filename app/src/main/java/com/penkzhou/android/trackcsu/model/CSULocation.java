package com.penkzhou.android.trackcsu.model;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

import java.util.Date;

/**
 * Created by Administrator on 2014/11/8.
 */
@AVClassName(CSULocation.CSULOCATION_CLASS)
public class CSULocation extends AVObject{
    static final String CSULOCATION_CLASS = "CSULocation";
    private static final String LOC_TIME_KEY = "locTime";
    private static final String LATITUDE_KEY = "latitude";
    private static final String LONGITUDE_KEY = "longitude";
    private static final String TIMEOUT_KEY = "timeout";
    private static final String USERNAME_KEY = "username";

    public Date getLocTime() {
        return this.getDate(LOC_TIME_KEY);
    }

    public void setLocTime(Date locTime) {
        this.put(LOC_TIME_KEY, locTime);
    }

    public double getLatitude(){
        return this.getDouble(LATITUDE_KEY);
    }

    public void setLatitude(double latitude) {
        this.put(LATITUDE_KEY, latitude);
    }

    public double getLongitude() {
        return this.getDouble(LONGITUDE_KEY);
    }

    public void setLongitude(double longitude) {
        this.put(LONGITUDE_KEY, longitude);
    }

    public String getUsername() {
        return this.getString(USERNAME_KEY);
    }

    public void setUsername(String username) {
        this.put(USERNAME_KEY, username);
    }


    public String getTimeout() {
        return this.getString(TIMEOUT_KEY);
    }

    public void setTimeout(String timeout) {
        this.put(TIMEOUT_KEY, timeout);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CSULocation && ((CSULocation) obj).getLongitude() == this.getLongitude() && ((CSULocation) obj).getLatitude() == this.getLatitude()) {
            return true;
        }
        return false;
    }
}
