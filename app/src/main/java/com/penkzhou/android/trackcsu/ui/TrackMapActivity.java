package com.penkzhou.android.trackcsu.ui;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

/**
 * Created by Administrator on 2014/11/8.
 */
public class TrackMapActivity extends SingleFragmentActivity {
    private static final String TAG = TrackMapActivity.class.getSimpleName();
    @Override
    protected Fragment createFragment() {
        Log.d(TAG,"createFragment");
        return new TrackMapFragment();
    }
}
